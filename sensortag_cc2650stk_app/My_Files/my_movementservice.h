/*
 * my_movementservice.h
 *
 *  Created on: Dec 30, 2016
 *      Author: Matthew
 */

#ifndef MY_FILES_MY_MOVEMENTSERVICE_H
#define MY_FILES_MY_MOVEMENTSERVICE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include "st_util.h"

/*********************************************************************
 * CONSTANTS
 */

// Service UUID
#define MOVEMENT_SERV_UUID             		0xAA80
#define MOVEMENT_DATA_UUID             		0xAA81
#define MY_MOVEMENT_DATA_UUID          		0xFF81
#define MOVEMENT_CONF_UUID             		0xAA82
#define MOVEMENT_PERI_UUID             		0xAA83
#define MY_MOVEMENT_DATA_NUM_SAMPLES_UUID   0xFF84

// Sensor Profile Services bit fields
#define MOVEMENT_SERVICE               0x00000020

// Length of sensor data in bytes
#define MOVEMENT_DATA_LEN              	  18
#define MY_MOVEMENT_DATA_MAX_NUM_SAMPLES  28 // 512/18 = 28.44

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * MACROS
 */


/*********************************************************************
 * API FUNCTIONS
 */


/*
 * Movement_addService - Initializes the Sensor GATT Profile service by
 *          registering GATT attributes with the GATT server.
 */
extern bStatus_t My_Movement_addService(void);

/*
 * Movement_registerAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t My_Movement_registerAppCBs(sensorCBs_t *appCallbacks);

/*
 * Movement_setParameter - Set a Sensor GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    len   - length of data to write
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 */
extern bStatus_t My_Movement_setParameter(uint8_t param, uint16_t len, void *value);

/*
 * Movement_getParameter - Get a Sensor GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to read.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 */
extern bStatus_t My_Movement_getParameter(uint8_t param, void *value);


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* MY_FILES_MY_MOVEMENTSERVICE_H */
