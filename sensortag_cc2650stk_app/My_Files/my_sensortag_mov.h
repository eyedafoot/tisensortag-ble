/*
 * my_sensortag_mov.h
 *
 *  Created on: Dec 31, 2016
 *      Author: Matthew
 */

#ifndef MY_FILES_MY_SENSORTAG_MOV_H
#define MY_FILES_MY_SENSORTAG_MOV_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include "sensortag.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

#ifndef EXCLUDE_MOV
/*
 * Initialize Movement sensor module
 */
extern void My_SensorTagMov_init(void);

/*
 * Task Event Processor for Movement sensor module
 */
extern void My_SensorTagMov_processSensorEvent(void);

/*
 * Task Event Processor for characteristic changes
 */
extern void My_SensorTagMov_processCharChangeEvt(uint8_t paramID);

/*
 * Reset Movement sensor module
 */
extern void My_SensorTagMov_reset(void);

#else

/* Movement module not included */

#define My_SensorTagMov_init()
#define My_SensorTagMov_processCharChangeEvt(paramID)
#define My_SensorTagMov_processSensorEvent()
#define My_SensorTagMov_reset()

#endif // EXCLUDE_MOV

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* MY_FILES_MY_SENSORTAG_MOV_H */
